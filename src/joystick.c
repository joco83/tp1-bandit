#include <stdint.h>
#include <stdio.h>

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include "joystick.h"
#include "uart.h"

uint32_t IntStatF = 0x0;

void EINT3_IRQHandler()
{
	IntStatF = LPC_GPIOINT->IO2IntStatF;

    if(IntStatF & (1 << 10))
    	uart0_send("Nord\r\n", 7);
    else if(IntStatF & (1 << 12))
    	uart0_send("Sud\r\n", 6);

    clrJoysticInt();
}

/*
 * Fonction d'initialisation du Joystic,
 */

void initJoystic(void)
{
    /*
     * Configuration des GPIOs en entrées.
     */
    LPC_GPIO2->FIODIR &= ~(1 << 10); // Nord.
    LPC_GPIO2->FIODIR &= ~(1 << 12); // Sud.

    /*
     * Déclanchement des interruptions sur le
     * flanc montant.
     */
    LPC_GPIOINT->IO2IntEnF |= (1 << 10); // Nord.
    LPC_GPIOINT->IO2IntEnF |= (1 << 12); // Sud.

    /*
     * Activation des interruptions des GPIO au
     * niveau di VIC.
     */
    NVIC_EnableIRQ(EINT3_IRQn);
}

/*
 * Fonction pour relâcher les interruptions liées
 * au joystic sur la carte LPC MyLab.
 */
void clrJoysticInt(void)
{
    LPC_GPIOINT->IO2IntClr |= (1 << 10); // Nord.
    LPC_GPIOINT->IO2IntClr |= (1 << 12); // Sud.
}
