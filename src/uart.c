#include <stdint.h>

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include "uart.h"

void uart0_init(uint32_t baudrate)
{
    uint32_t Fdiv;
    uint32_t pclkdiv, pclk;

    LPC_PINCON->PINSEL0 &= ~0x000000f0;
    LPC_PINCON->PINSEL0 |=  0x00000050;

    pclkdiv = ((LPC_SC->PCLKSEL0 >> 6) & 0x03);

    switch(pclkdiv)
    {
        case 0x00:
        default:
            pclk = (SystemCoreClock / 4);
            break;

        case 0x01:
            pclk = SystemCoreClock;
            break;

        case 0x02:
            pclk = (SystemCoreClock / 2);
            break;

        case 0x03:
            pclk = (SystemCoreClock / 8);
            break;
    }

    LPC_UART0->LCR = 0x83;
    Fdiv = ((pclk / 16) / baudrate);
    LPC_UART0->DLM = (Fdiv / 256);
    LPC_UART0->DLL = (Fdiv % 256);
    LPC_UART0->LCR = 0x03;
    LPC_UART0->FCR = 0x07;
}

void uart0_send(uint8_t *data, uint32_t length)
{
    volatile uint32_t i = 0;

    for(i = 0; i < length; i++)
    {
        /*
         * On boucle tantque le registre THR n'est pas vide.
         */
        while(!(LPC_UART0->LSR & (1 << 5)));

        LPC_UART0->THR = data[i];
    }
}
