#ifndef _BANDITMANCHOT_H_
#define _BANDITMANCHOT_H_

#include <stdint.h>

#define USERTASK_STACK_SIZE (unsigned short)80

/*
 * Nombre de roues dans le jeu.
 */
#define NB_WHEEL 0x3

/*
 * Constantes qui définissent l'index de chaques tâches
 * associées à une roue.
 */
#define IDX_TASK_1 0x0
#define IDX_TASK_2 0x1
#define IDX_TASK_3 0x2

/*
 * Macro permettant de définir le delai de changement
 * d'état d'une roue.
 */

#define DELAY_WHEEL(idx_task) \
	(50 + (idx_task * 5))

typedef struct
{
	uint8_t idx;
	uint8_t value;
}p_task_wheel;

extern p_task_wheel **lw;
extern xSemaphoreHandle *lsem;

/*
 * Prototypes des tâches.
 */
void task_wheel(void *param);
void task_print_uart(void *param);

/*
 * Prototypes des fonctions.
 */
void init_task_param(uint32_t nb_task);
void init_sem(uint32_t nb_wheel);
void release_sem();
#endif
