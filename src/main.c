/**
*  Created on  : 19.01.2014
 * Author      : VP
 * Description : exercice 1 of serie 4
 */


#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <stdio.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "uart.h"
#include "BanditManchot.h"
#include "joystick.h"


int main(void)
{
	char task_name[10];

	initJoystic();
	uart0_init(115200);
	init_task_param(NB_WHEEL);

	uart0_send("** PLAY TO WIN **\r\n\r\n", 22);

	/*
	 * Création de N tâches.
	 */

	for(int i = 0; i < NB_WHEEL; i++)
	{
		sprintf(task_name, "%d", i);
		xTaskCreate(task_wheel, (const signed portCHAR *)task_name, USERTASK_STACK_SIZE, lw[i], tskIDLE_PRIORITY + 1, NULL);
	}

	xTaskCreate(task_print_uart, NULL, USERTASK_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);

	vTaskStartScheduler();

	return 1;
}
