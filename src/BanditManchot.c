#include <stdint.h>
#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "BanditManchot.h"

p_task_wheel **lw = NULL;
xSemaphoreHandle *lsem = NULL;

void init_task_param(uint32_t nb_task)
{
	lw = pvPortMalloc(nb_task * sizeof(p_task_wheel *));

	for(uint32_t i = 0; i < nb_task; i++)
	{
		lw[i]        = pvPortMalloc(sizeof(p_task_wheel));
		lw[i]->idx   = i;
		lw[i]->value = 0x0;
	}
}

void task_wheel(void *param)
{
	p_task_wheel *pt = (p_task_wheel *)param;

	// Prise du sémaphore du thread initialisé à 0.
	xSemaphoreTake(lsem[pt->idx], portMAX_DELAY);

    while(1)
    {
    	if(pt->value == 9)
    		pt->value = 0;
    	else
    		pt->value++;
    	vTaskDelay((500 + (pt->idx * 50)) / portTICK_RATE_MS);
    }
}

void task_print_uart(void *param)
{
	char task_value[20];

	while(1)
	{
		uart0_send(" | ", 4);

		for(int i = 0; i < NB_WHEEL; i++)
		{
			sprintf(task_value, "%d", lw[i]->value);

			uart0_send(task_value, strlen(task_value) + 1);
			uart0_send(" | ", 4);

			lw[i]->value;
		}
		uart0_send("\r", 1);

		vTaskDelay(100 / portTICK_RATE_MS);
	}
}

/*
 * Création des sémaphores.
 * Une sémaphore par roue.
 */
void init_sem(uint32_t nb_wheel) {

	lsem = pvPortMalloc(nb_wheel * sizeof(xSemaphoreHandle));

	for (uint32_t i = 0; i < nb_wheel; ++i) {

		lsem[i] = pvPortMalloc(sizeof(xSemaphoreHandle));
		vSemaphoreCreateBinary(lsem[i]);



		if ( lsem[i] == NULL ) {
			/*  There was insufficient OpenRTOS heap available for the semaphore to be created successfully. */
		}
	}
}

/*
 * Relacher toutes les sémaphores pour démarer le jeux
 */
void release_sem() {

	static signed BaseType_t xHigherPriorityTaskWoken;
	xHigherPriorityTaskWoken = pdFALSE; // Ne pas interrompre l'interruption

	for (int i = 0; i < NB_WHEEL; ++i) {
		xSemaphoreGiveFromISR(lsem[i], &xHigherPriorityTaskWoken);
	}
}

